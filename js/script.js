var SCREEN_WIDTH = window.innerWidth;
var SCREEN_HEIGHT = window.innerHeight;

var container,stats;
var camera, scene, renderer;
var mouse3D, isMouseDown = false, onMouseDownPosition,
radious = 1600, theta = 45, onMouseDownTheta = 45, phi = 60, onMouseDownPhi = 60,
isShiftDown = false;

var clock = new THREE.Clock();

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var mouseX = 0, mouseY = 0;

init();
animate();

function init() {
	container = document.createElement( 'div' );
	document.body.appendChild( container );

	// CAMERA
	camera = new THREE.PerspectiveCamera( 30, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 10000 );
	camera.position.x = 200;
	camera.position.y = 30; // camera upper
	camera.position.z = 570;

	// SCENE
	scene = new THREE.Scene();
	scene.fog = new THREE.Fog( 0x0c5ca4, 1000, 10000 );

	// CONTROLS
	controls = new THREE.OrbitControls( camera );
	controls.maxPolarAngle = Math.PI/2;  // do not go below the ground


	// LIGHTS
	var directionalLight = new THREE.DirectionalLight( 0x333333, 2 );
	directionalLight.position.set( 100, 100, -100 );
	scene.add( directionalLight );

	var hemiLight = new THREE.HemisphereLight( 0xaabbff, 0x040404, 1 );

	hemiLight.position.y = 500;
	scene.add( hemiLight );

	// RENDERER
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setClearColor( scene.fog.color );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
	renderer.domElement.style.position = "relative";
	container.appendChild( renderer.domElement );

	renderer.gammaInput = true;
	renderer.gammaOutput = true;

	// GROUND
	var maxAnisotropy = renderer.getMaxAnisotropy();

	var texture1 = THREE.ImageUtils.loadTexture( "textures/honeycomb.png" );
	var material1 = new THREE.MeshPhongMaterial( { color: 0x0c5ca4, map: texture1 } );

	texture1.anisotropy = maxAnisotropy;
	texture1.wrapS = texture1.wrapT = THREE.RepeatWrapping;
	texture1.repeat.set( 512, 512 );

	var geometry = new THREE.PlaneBufferGeometry( 100, 100 );

	var mesh1 = new THREE.Mesh( geometry, material1 );
	mesh1.position.y = - 100;
	mesh1.rotation.x = - Math.PI / 2;
	mesh1.scale.set( 1000, 1000, 1000 );
	scene.add( mesh1 );

	// STATS
	stats = new Stats();
	container.appendChild( stats.domElement );

	// MODEL
	var loader = new THREE.OBJMTLLoader();
	loader.load( 'obj/eatonswitchgear.obj', 'obj/eatonswitchgear.mtl', function ( object ) {
	object.position.y = - 100;
	object.position.x = - -40;
	scene.add( object );
	} );

	// load info buttons
	var texture2 = THREE.ImageUtils.loadTexture( "textures/info.png" );
	var material2 = new THREE.MeshPhongMaterial( { color: 0xffffff, map: texture2, transparent: true } );
	var geometry2 = new THREE.PlaneBufferGeometry( 100, 100 );
	
	// load info boxes
	generateInfo(geometry2, material2, scene);

	window.addEventListener( 'resize', onWindowResize, false );
	window.addEventListener( 'mousedown', onDocumentMouseDown, false );
	//window.addEventListener( 'mousemove', onDocumentMouseMove, false );
}

function generateInfo(geometry2, material2, scene){
	var cube = new THREE.Mesh( geometry2, material2 );	
	cube.position.set(-65,53,95);
	scene.add( cube );
	cube.scale.x = 0.10;
	cube.scale.y = 0.10; 
	cube.scale.z = 0.12;
	cube.name = 'secondarycompartment'; 
	cube.callback = function() { 
		jQuery(document).ready(function() {
		    jQuery.fancybox(
		        '<h2>Secondary Compartment</h2><p>Devices such as control pushbuttons,<br/> indicating lights, switches and analog<br/> meters can be mounted on these panels.</p>',
		        {
		            'autoDimensions'    : false,
		            'width'             : 300,
		            'height'            : 'auto',
		            'transitionIn'      : 'none',
		            'transitionOut'     : 'none'
		        }
		    );
		});
	}

	var cube2 = new THREE.Mesh( geometry2, material2 );
	cube2.position.set(-81,28,95);
	scene.add( cube2 );
	cube2.scale.x = 0.10; 
	cube2.scale.y = 0.10; 
	cube2.scale.z = 0.12; 
	cube2.name = 'breakerprovision'; 
	cube2.callback = function() { 
		jQuery(document).ready(function() {
		    jQuery.fancybox(
		        '<h2>Breaker Provision</h2><p>This is a prepared cell for future use.<br/> All required parts are installed and wired.</p>',
		        {
		            'autoDimensions'    : false,
		            'width'             : 300,
		            'height'            : 'auto',
		            'transitionIn'      : 'none',
		            'transitionOut'     : 'none'
		        }
		    );
		});
	}

	var cube3 = new THREE.Mesh( geometry2, material2 );
	cube3.position.set(36,19,95);
	scene.add( cube3 );
	cube3.scale.x = 0.10; 
	cube3.scale.y = 0.10; 
	cube3.scale.z = 0.12; 
	cube3.name = 'throughthedoor'; 
	cube3.callback = function() { 
		jQuery(document).ready(function() {
		    jQuery.fancybox(
		        '<h2>Through-the-door design</h2><p>This design allowed several function to be performed without the need to open the circuitbreaker door<ul style="list-type:none;"><li>Lever the breaker between positions</li><li>Operate manual charging system while viewing the spring charge status flag</li><li>Close and open breaker</li><li>View and adjust trip unit settings</li><li>Read the breaker rating nameplate</li></ul>.</p>',
		        {
		            'autoDimensions'    : false,
		            'width'             : 200,
		            'height'            : 'auto',
		            'transitionIn'      : 'none',
		            'transitionOut'     : 'none'
		        }
		    );
		});
	}

	var cube4 = new THREE.Mesh( geometry2, material2 );
	cube4.position.set(36,75,95);
	scene.add( cube4 );
	cube4.scale.x = 0.10; 
	cube4.scale.y = 0.10; 
	cube4.scale.z = 0.12; 
	cube4.name = 'instrumentcompartmentdoor'; 
	cube4.callback = function() { 
		jQuery(document).ready(function() {
		    jQuery.fancybox(
		        '<h2>Instrument compartment door</h2><p>Devices, such us electronic power metering and analog switchboard type meters that do not fit on the secondary terminal compartment door or on a panel of a blank cell. Voltage transformer, control power transformers and associated fuse blocks and breakers are located within the instrument compartment.</p>',
		        {
		            'autoDimensions'    : false,
		            'width'             : 200,
		            'height'            : 'auto',
		            'transitionIn'      : 'none',
		            'transitionOut'     : 'none'
		        }
		    );
		});
	}

	var cube5 = new THREE.Mesh( geometry2, material2 );
	cube5.position.set(110,75,95);
	cube5.rotation.y = - Math.PI / 8;
	scene.add( cube5 );
	cube5.scale.x = 0.10; 
	cube5.scale.y = 0.10; 
	cube5.scale.z = 0.12; 
	cube5.name = 'flexconnectors'; 
	cube5.callback = function() { 
		jQuery(document).ready(function() {
		    jQuery.fancybox(
		        '<h2>Flex connectors</h2><p>For close coupling to a dry type transformer, this option can be reconfigured to accomodate a liquid field transformer as well. This gear can also be reconfigured to close couple to low voltage MCC, switchboard, and automatic transfer switch.</p>',
		        {
		            'autoDimensions'    : false,
		            'width'             : 200,
		            'height'            : 'auto',
		            'transitionIn'      : 'none',
		            'transitionOut'     : 'none'
		        }
		    );
		});
	}
}
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
function onDocumentMouseDown( event ) {

    event.preventDefault();

    mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( scene.children ); 
    if ( intersects.length > 0 ) {
    	if ( intersects[ 0 ].object.name ){
			intersects[0].object.callback();
    	}
    }
}
/*function onDocumentMouseMove(event) {

	//mouseX = ( event.clientX - windowHalfX );
	mouseY = ( event.clientY - windowHalfY );

}*/

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );
}

//

function animate() {
	requestAnimationFrame( animate );

	//camera.position.x += ( mouseX - camera.position.x ) * .15;
	//camera.position.y = THREE.Math.clamp( camera.position.y + ( - ( mouseY - 200 ) - camera.position.y ) * .05, 50, 1000 );

	camera.lookAt( scene.position );

	renderer.render( scene, camera );
	stats.update();
}